package com.example.myapplication.customWidgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatTextView;

import com.example.myapplication.DownloadFileApplication;
import com.example.myapplication.R;


public class CustomTextView extends AppCompatTextView {
    private int defaultDimension = 0;
    private int fontName;
    private int DINOffcPro = 0;
    private int DINOffcProBold = 1;
    private int DINOffcProCondMedi = 2;
    private int DINOffcProIta = 3;
    private int DINOffcProLight = 4;
    private int DINOffcProLightIta = 5;
    private int DINPro = 6;
    private int DINProBold = 7;
    private int DINProCondmedium = 8;
    private int DINProItalic = 9;
    private int DINProLight = 10;
    private int DINProLightItalic = 11;

    public CustomTextView(Context context) {
        super(context);
        init(null, 0);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextViewStyle, defStyle, 0);
        fontName = a.getInt(R.styleable.TextViewStyle_customTypeFace, defaultDimension);
        Drawable drawableLeft = null;
        Drawable drawableRight = null;
        Drawable drawableBottom = null;
        Drawable drawableTop = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawableLeft = a.getDrawable(R.styleable.TextViewStyle_drawableLeftCompat);
            drawableRight = a.getDrawable(R.styleable.TextViewStyle_drawableRightCompat);
            drawableBottom = a.getDrawable(R.styleable.TextViewStyle_drawableBottomCompat);
            drawableTop = a.getDrawable(R.styleable.TextViewStyle_drawableTopCompat);
        } else {
            final int drawableLeftId = a.getResourceId(R.styleable.TextViewStyle_drawableLeftCompat, -1);
            final int drawableRightId = a.getResourceId(R.styleable.TextViewStyle_drawableRightCompat, -1);
            final int drawableBottomId = a.getResourceId(R.styleable.TextViewStyle_drawableBottomCompat, -1);
            final int drawableTopId = a.getResourceId(R.styleable.TextViewStyle_drawableTopCompat, -1);

            if (drawableLeftId != -1)
                drawableLeft = AppCompatResources.getDrawable(getContext(), drawableLeftId);
            if (drawableRightId != -1)
                drawableRight = AppCompatResources.getDrawable(getContext(), drawableRightId);
            if (drawableBottomId != -1)
                drawableBottom = AppCompatResources.getDrawable(getContext(), drawableBottomId);
            if (drawableTopId != -1)
                drawableTop = AppCompatResources.getDrawable(getContext(), drawableTopId);
        }
        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);
        a.recycle();
        DownloadFileApplication application = (DownloadFileApplication) getContext().getApplicationContext();
        if (fontName == DINOffcPro) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINOffcPro.ttf"));
        } else if (fontName == DINOffcProBold) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINOffcPro-Bold.ttf"));
        } else if (fontName == DINOffcProCondMedi) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINOffcPro-CondMedi.ttf"));
        } else if (fontName == DINOffcProIta) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINOffcPro-Ita.ttf"));
        } else if (fontName == DINOffcProLight) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINOffcPro-Light.ttf"));
        } else if (fontName == DINOffcProLightIta) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINOffcPro-LightIta.ttf"));
        } else if (fontName == DINPro) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINPro.otf"));
        } else if (fontName == DINProBold) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINPro-Bold.otf"));
        } else if (fontName == DINProCondmedium) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINPro-CondMedium.otf"));
        } else if (fontName == DINProItalic) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINPro-Italic.otf"));
        } else if (fontName == DINProLight) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINPro-Light.otf"));
        } else if (fontName == DINProLightItalic) {
            setTypeface(Typeface.createFromAsset(getContext().getAssets(), "DINPro-LightItalic.otf"));
        }
    }
}