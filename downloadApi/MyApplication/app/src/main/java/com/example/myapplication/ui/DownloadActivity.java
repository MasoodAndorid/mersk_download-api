package com.example.myapplication.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.DownloadFileApplication;
import com.example.myapplication.R;
import com.example.myapplication.base.BaseActivity;
import com.example.myapplication.connectivity.Connectivity;
import com.example.myapplication.databinding.ActivityMainBinding;
import com.example.myapplication.viewModel.DownloadViewModel;
import com.example.myapplication.webService.DownloadService;

import org.apache.commons.io.FilenameUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

/**
 * @author shaikm16
 * date Aug 06 2019
 */
public class DownloadActivity extends BaseActivity {

    private static final String TAG = DownloadActivity.class.getCanonicalName();
    private DownloadViewModel mDownloadViewModel;
    private ActivityMainBinding mActivityBinding;
    Activity mActivity;

    /**
     *
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = DownloadActivity.this;
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(mActivityBinding.toolBar.toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        }
        mActivityBinding.toolBar.toolbarTitle.setText(getString(R.string.toolobar_name));
        mActivityBinding.downloadProgress.setText(getString(R.string.download_progress));
        mActivityBinding.downloadProgress.setVisibility(View.GONE);
        mActivityBinding.progressbar.setVisibility(View.GONE);
        mActivityBinding.editText.setText("https://api.androidhive.info/progressdialog/hive.jpg");
        askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 101);
        mDownloadViewModel = ViewModelProviders.of(this).get(DownloadViewModel.class);
        dowmloadApiRequest();
        downloadApiRespomse();
    }

    private void dowmloadApiRequest() {

        mActivityBinding.submit.setOnClickListener(view -> {

            if(Connectivity.isConnected(this)) {
                if(Patterns.WEB_URL.matcher(mActivityBinding.editText.getText().toString()).matches()) {
                    URL url = null;
                    try {
                        url = new URL(mActivityBinding.editText.getText().toString());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    callApi(mActivityBinding.editText.getText().toString(), FilenameUtils.getName(url.getPath()));
                    Log.d(TAG, FilenameUtils.getBaseName(url.getPath()));
                    Log.d(TAG, FilenameUtils.getExtension(url.getPath()));
                    Log.d(TAG, FilenameUtils.getName(url.getPath()));
                }else{
                    showToast(getString(R.string.url_not_valid));
                }
            }else{
                showToast(getString(R.string.no_internet_msg));
            }
        });
    }

    private void downloadApiRespomse(){

        mDownloadViewModel.getmDownloadFile().observe(this, s -> {
            mActivityBinding.downloadProgress.setVisibility(View.VISIBLE);
            mActivityBinding.progressbar.setVisibility(View.VISIBLE);
        });

        mDownloadViewModel.getmDownloadProgress().observe(this, integer -> {
            Log.d(TAG, "My downloadProgress "+integer);
            mActivityBinding.progressbar.setProgress(integer);

            if(integer != 100)
                mActivityBinding.downloadProgress.setText(getString(R.string.progress)+" " + integer + "%");
            else if(integer == 100)
                mActivityBinding.downloadProgress.setText(getString(R.string.download_completed));
            else if(integer == -1)
                mActivityBinding.downloadProgress.setText(getString(R.string.download_failed));
        });
    }
    /**
     *
     * @param text
     * @param fileName
     */
    private void callApi(String text, String fileName){

        if(text.trim().length()> 0){
            DownloadService mDownloadService = ((DownloadFileApplication) getApplication()).getmApiConnectionModlue().getDownloadService();
            mDownloadViewModel.downloadApi(mDownloadService,text, fileName);
        }else{
            showToast(getString(R.string.please_give_valid_url));
        }
    }

    /**
     *
     * @param permission
     * @param requestCode
     */
    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(DownloadActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(DownloadActivity.this, permission)) {
                ActivityCompat.requestPermissions(DownloadActivity.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(DownloadActivity.this, new String[]{permission}, requestCode);
            }
        } else if (ContextCompat.checkSelfPermission(DownloadActivity.this, permission) == PackageManager.PERMISSION_DENIED) {
           showToast(getString(R.string.permission_denied));
        }
    }

    /**
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {

            if (requestCode == 101)
                showToast(getString(R.string.permission_granted));
        } else {
            showToast(getString(R.string.permission_denied));

        }
    }

    @Override
    protected void onNetworkConnectionChanged(boolean isConnected) {
        Log.d(TAG, "NetworkConnection" + isConnected);
        if (!isConnected) {
            showToast(getString(R.string.internet_changed));
        }
    }
}

