package com.example.myapplication.webService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiConnectionModule {
    private static volatile ApiConnectionModule retrofitManager;
    private static Retrofit retrofit;
    private static String mBaseUrl="https://your.api.url/";
    private static DownloadService mDownloadService;


    public static ApiConnectionModule getInstance() {
        if (null == retrofitManager) {
            synchronized (ApiConnectionModule.class) {
                if (retrofitManager == null) {
                    retrofitManager = new ApiConnectionModule();
                    mDownloadService = retrofit.create(DownloadService.class);
                }
            }
        }
        return retrofitManager;
    }

    public static void changeApiBaseUrl(String newApiBaseUrl) {
        mBaseUrl = newApiBaseUrl;
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(mBaseUrl).build();
    }

    public ApiConnectionModule() {
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create()).baseUrl(mBaseUrl)
                .build();
    }

    public static DownloadService getDownloadService() {
        return mDownloadService;
    }


}
