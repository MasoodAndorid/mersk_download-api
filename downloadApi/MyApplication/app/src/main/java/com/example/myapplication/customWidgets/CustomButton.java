package com.example.myapplication.customWidgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.LruCache;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatButton;

import com.example.myapplication.R;


public class CustomButton extends AppCompatButton {

    private static LruCache<String, Typeface> sTypefaceCache = new LruCache<>(12);

    public CustomButton(Context context) {
        super(context);
        init(null, 0);
        //  init();
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
        // init();
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
        // init();
    }


    public void init() {
        Typeface typeface = sTypefaceCache.get("FONTAWESOME");
        if (typeface == null) {
            typeface = Typeface.createFromAsset(getContext().getAssets(), "DINPro-Bold.otf");
            sTypefaceCache.put("FONTAWESOME", typeface);
        }
        setTypeface(typeface);
    }


    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TextViewStyle, defStyle, 0);

        Drawable drawableLeft = null;
        Drawable drawableRight = null;
        Drawable drawableBottom = null;
        Drawable drawableTop = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawableLeft = typedArray.getDrawable(R.styleable.TextViewStyle_drawableLeftCompat);
            drawableRight = typedArray.getDrawable(R.styleable.TextViewStyle_drawableRightCompat);
            drawableBottom = typedArray.getDrawable(R.styleable.TextViewStyle_drawableBottomCompat);
            drawableTop = typedArray.getDrawable(R.styleable.TextViewStyle_drawableTopCompat);
        } else {
            final int drawableLeftId = typedArray.getResourceId(R.styleable.TextViewStyle_drawableLeftCompat, -1);
            final int drawableRightId = typedArray.getResourceId(R.styleable.TextViewStyle_drawableRightCompat, -1);
            final int drawableBottomId = typedArray.getResourceId(R.styleable.TextViewStyle_drawableBottomCompat, -1);
            final int drawableTopId = typedArray.getResourceId(R.styleable.TextViewStyle_drawableTopCompat, -1);

            if (drawableLeftId != -1)
                drawableLeft = AppCompatResources.getDrawable(getContext(), drawableLeftId);
            if (drawableRightId != -1)
                drawableRight = AppCompatResources.getDrawable(getContext(), drawableRightId);
            if (drawableBottomId != -1)
                drawableBottom = AppCompatResources.getDrawable(getContext(), drawableBottomId);
            if (drawableTopId != -1)
                drawableTop = AppCompatResources.getDrawable(getContext(), drawableTopId);
        }
        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);
        typedArray.recycle();
        Typeface typeface = sTypefaceCache.get("FONTAWESOME");
        if (typeface == null) {
            typeface = Typeface.createFromAsset(getContext().getAssets(), "DINPro-Bold.otf");
            sTypefaceCache.put("FONTAWESOME", typeface);
        }
        setTypeface(typeface);
    }
}
