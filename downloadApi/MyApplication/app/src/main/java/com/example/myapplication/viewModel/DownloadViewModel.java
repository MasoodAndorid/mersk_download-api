package com.example.myapplication.viewModel;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.myapplication.webService.DownloadService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownloadViewModel extends ViewModel {

    private static final String TAG = DownloadService.class.getCanonicalName();
    private MutableLiveData<String> mDownloadFile = new MutableLiveData<>();
    private MutableLiveData<Integer> mDownloadProgress = new MutableLiveData<>();
    private DownloadZipFileTask downloadZipFileTask;
    private String fileName;

    public void setmDownloadFile(String mDownloadFile) {
        this.mDownloadFile.postValue(mDownloadFile);
    }

    public void setProgress(int progress){
        this.mDownloadProgress.postValue(progress);
    }

    public MutableLiveData<Integer> getmDownloadProgress() {
        return mDownloadProgress;
    }

    public MutableLiveData<String> getmDownloadFile() {
        return mDownloadFile;
    }

    /**
     *
     * @param mService
     * @param text
     * @param fileName
     */

    public void downloadApi(DownloadService mService, String text, final String fileName){
        this.fileName = fileName;

        if(mService != null){

            mService.downloadFileByUrl(text).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    if(response.body() != null){
                            setmDownloadFile(response.body().toString());
                            Log.d(TAG, "My Response body is "+response.body().toString());
                        downloadZipFileTask = new DownloadZipFileTask();
                        downloadZipFileTask.execute(response.body());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    Log.d(TAG, "on Failure occured");
                }
            });
        }
    }

    /**
     *
     * @param body
     * @param filename
     */
    private void saveToDisk(ResponseBody body, String filename) {
        try {

            File destinationFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(destinationFile);
                byte data[] = new byte[4096];
                int count;
                int progress = 0;
                long fileSize = body.contentLength();
                Log.d(TAG, "File Size=" + fileSize);
                while ((count = inputStream.read(data)) != -1) {
                    outputStream.write(data, 0, count);
                    progress += count;
                    Pair<Integer, Long> pairs = new Pair<>(progress, fileSize);
                    downloadZipFileTask.doProgress(pairs);
                    Log.d(TAG, "Progress: " + progress + "/" + fileSize + " >>>> " + (float) progress / fileSize);
                }

                outputStream.flush();

                Log.d(TAG, Objects.requireNonNull(destinationFile.getParent()));
                Pair<Integer, Long> pairs = new Pair<>(100, 100L);
                downloadZipFileTask.doProgress(pairs);
                return;
            } catch (IOException e) {
                e.printStackTrace();
                Pair<Integer, Long> pairs = new Pair<>(-1, Long.valueOf(-1));
                downloadZipFileTask.doProgress(pairs);
                Log.d(TAG, "Failed to save the file!");
                return;
            } finally {
                if (inputStream != null) inputStream.close();
                if (outputStream != null) outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "Failed to save the file!");
            return;
        }
    }

    private class DownloadZipFileTask extends AsyncTask<ResponseBody, Pair<Integer, Long>, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(ResponseBody... urls) {
            //Copy you logic to calculate progress and call
            saveToDisk(urls[0], fileName);
            return null;
        }

        protected void onProgressUpdate(Pair<Integer, Long>... progress) {

            Log.d("API123", progress[0].second + " ");

            if (progress[0].first == 100) {
               //Toast.makeText(getApplicationContext(), "File downloaded successfully", Toast.LENGTH_SHORT).show()
            }

            if (progress[0].second > 0) {
                int currentProgress = (int) ((double) progress[0].first / (double) progress[0].second * 100);
                setProgress(currentProgress);
             // progressBar.setProgress(currentProgress);
              //txtProgressPercent.setText("Progress " + currentProgress + "%");

            }

            if (progress[0].first == -1) {
                setProgress(-1);
               // Toast.makeText(getApplicationContext(), "Download failed", Toast.LENGTH_SHORT).show();
            }

        }

        public void doProgress(Pair<Integer, Long> progressDetails) {
            publishProgress(progressDetails);
        }

        @Override
        protected void onPostExecute(String result) {

        }
    }
}

