package com.example.myapplication;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.example.myapplication.webService.ApiConnectionModule;

public class DownloadFileApplication extends MultiDexApplication {

    private ApiConnectionModule mApiConnectionModlue;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        mApiConnectionModlue = ApiConnectionModule.getInstance();

    }

    public ApiConnectionModule getmApiConnectionModlue(){
        return mApiConnectionModlue;
    }


}
